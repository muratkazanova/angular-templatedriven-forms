import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeRoutingModule } from './employee-routing.module';
import { EmployeeFormComponent } from './Components/employee-form/employee-form.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [EmployeeFormComponent],
  imports: [CommonModule, SharedModule, EmployeeRoutingModule]
})
export class EmployeeModule {}
