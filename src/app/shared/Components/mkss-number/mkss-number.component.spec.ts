import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkssNumberComponent } from './mkss-number.component';

describe('MkssNumberComponent', () => {
  let component: MkssNumberComponent;
  let fixture: ComponentFixture<MkssNumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkssNumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkssNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
